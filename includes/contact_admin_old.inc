<?php
/**
 * @file
 * Admin page callback file for the user module.
 *
 */
/**
 * Page callback: Generates the appropriate user administration form.
 */
function my_contacts_contact_people_directory() {
  global $base_url, $user;
  $status = ((arg(1) == 'status' && arg(2) == '') || (arg(1) == 'status' && arg(2) == 1)) ? 1 : 0;
  $user_roles = my_contacts_get_user_roles();
  $intersection = array_intersect($user_roles, $user->roles);
  $intersection_count = count($intersection);
  $alldiruser = my_contacts_getalldiruser($status, '', $user_roles);
  $uid = $user->uid;
  $userslist = my_contacts_getfriendlist($uid);
  $users_status_list = my_contacts_getfriends_statuslist($uid);
  $form = array();
  $form['total_column'] = array(
    '#type' => 'hidden',
    '#value' => '5',
  );
  $imgpath = check_url($base_url . '/' . drupal_get_path('module', 'my_contacts'));
  $add_others_contact = "<img src='$imgpath/images/users_search.png'  title = 'Pull Other User Contacts'/>";
  if (!empty($alldiruser)) {
    foreach ($alldiruser as $key => $val) {
      $form['first_name' . $val['contact_id']] = array(
        '#type' => 'markup',
        '#value' => l($val['first_name'], 'view-contact' . '/' . $val['contact_id']),
      );
      $form['last_name' . $val['contact_id']] = array(
        '#type' => 'markup',
        '#value' => $val['last_name'],
      );
      $form['email1' . $val['contact_id']] = array(
        '#type' => 'markup',
        '#value' => l($val['email1'], 'mailto:' . $val['email1']),
      );
      $form['organization' . $val['contact_id']] = array(
        '#type' => 'markup',
        '#value' => $val['organization'],
      );
      if (in_array($val['contact_id'], $userslist)) {
        $defaultclass = 'remove_to_contacts';
        $imgpaths = $imgpath . '/images/remove-contact.png';
        $title  = 'Remove From Contact';
      }
      else {
        $defaultclass = 'add_to_contacts';
        $imgpaths = $imgpath . '/images/add-contact.png';
        $title  = 'Add To Contact';
      }
      if (in_array($val['contact_id'], $users_status_list)) {
        $statusclass = 'Private';
        $status_path = $imgpath . '/images/private.png';
        $status_title  = 'Private';
      }
      else {
        $statusclass = 'Public';
        $status_path = $imgpath . '/images/public.png';
        $status_title  = 'Public';
      }
      $status_img = "<img class='$statusclass' src='$status_path' title= '$status_title' />";
      $add_img = "<img class='$defaultclass' src='$imgpaths' title= '$title' />";
      $view_icon_img = "<img src='$imgpath/images/View_icon.png' height='16px' width='16px' title='View' />";
      $edit_contact_img = "<img src='$imgpath/images/edit.png' title='Edit' />";
      $remov_contact_img = "<img src='$imgpath/images/Trash.png' height='16px' width='16px' title='Delete'/>";
      $edit_contact_value = $user->uid == $val['uid'] || $intersection_count > 0 ? '|' . l($edit_contact_img, 'edit-contact/' . $val['contact_id'], array('html' => 'true')) : '';
      $revmove_contact_value = $user->uid == $val['uid'] || $intersection_count > 0 ? l($remov_contact_img, '', array('fragment' => 'remove_to_contact', 'external' => TRUE, 'html' => 'true', 'attributes' => array('class' => 'user_remove', 'rel' => $val["contact_id"], 'onclick' => 'remove_permanat_contact(this);'))) : l($add_img, ' ', array('fragment' => 'add_to_contact', 'external' => TRUE, 'html' => 'true', 'attributes' => array('id' => $val['contact_id'], 'class' => 'add_to_contacts', 'onclick' => 'rd_contacts(this);')));
      $manage_contact = $intersection_count > 0 || $user->uid == 1 ? '|' . l($add_img, ' ', array('fragment' => 'add_to_contact', 'external' => TRUE, 'html' => 'true', 'attributes' => array('id' => $val['contact_id'], 'class' => 'add_to_contacts', 'onclick' => 'rd_contacts(this);'))) : '';
      $form['action' . $val['contact_id']] = array(
        '#type' => 'markup',
        '#value' => $revmove_contact_value .'|' . l($view_icon_img, 'view-contact/' . $val['contact_id'], array('html' => 'true')) .
        '|' . l($status_img, ' ', array('fragment' => 'contact_status', 'external' => TRUE, 'html' => 'true', 'attributes' => array( 'id' => 'status_' . $val['contact_id'], 'class' => 'contact_status', 'onclick' => 'pub_prv_contact(this);'))) .  $edit_contact_value . $manage_contact,
      );
    }
    // Add to contact dialog box form.
    $form['add_to_contact_form'] = array(
      '#type' => 'markup',
      '#value' => my_contacts_confirmdialogbox(),
    );
    $form['rem_to_contact_form'] = array(
      '#type' => 'markup',
      '#value' => my_contacts_remove_confirmdialogbox(),
    );
    $form['add_user_friends_list'] = array(
      '#type' => 'textfield',
      '#title' => t('Add as contact') . '<span id="requesteeid_frnd"></span>',
      '#size' => '35',
      '#autocomplete_path' => 'friends/autocomplete',
    );
    // Show My Account checkbox.
    $form['show_myaccount'] = array(
      '#type' => 'checkbox',
      '#title' => t('My Contact'),
      '#value' => (arg(1) == 'status' && arg(2) == '' || (arg(1) == 'status' && arg(2) == 1)) ? 1 : 0,
    );
    $form['add_more_contacts'] = array(
      '#type' => 'markup',
      '#title' => t('My Contact'),
      '#value' => l($add_others_contact, '', array('fragment' => 'add_to_others_contact', 'html' => 'true', 'external' => TRUE, 'attributes' => array('id' => $val['contact_id'], 'class' => 'add_to_contacts_friends'))),
    );
    return $form;
  }
  else {
    $form['show_myaccount'] = array(
      '#type' => 'checkbox',
      '#title' => t('My Contact'),
      '#value' => (arg(1) == 'status' && arg(2) == '' || (arg(1) == 'status' && arg(2) == 1)) ? 1 : 0,
    );
    $form['add_more_contacts'] = array(
      '#type' => 'markup',
      '#title' => t('My Contact'),
      '#value' => l($add_others_contact, '', array('fragment' => 'add_to_others_contact', 'html' => 'true', 'external' => TRUE, 'attributes' => array('id' => $val['contact_id'], 'class' => 'add_to_contacts_friends'))),
    );
    $form['add_user_friends_list'] = array(
      '#type' => 'textfield',
      '#title' => t('Add as contact') . '<span id="requesteeid_frnd"></span>',
      '#size' => '35',
      '#autocomplete_path' => 'friends/autocomplete',
    );
    $form['no_records'] = array(
      '#type' => 'markup',
      '#value' => '',
    );
    return $form;
  }
}
/**
 * Page callback: Add contact in user directory.
 */
function my_contacts_add_contact_user_directory() {
  global $user;
  $requesteeuser = $_GET['requesteeid'];
  $requesterid = $user->uid;
  $contactstatus = $_GET['contact_status'];
  $state = my_contacts_handlingusercontacts($requesterid, $requesteeuser, $contactstatus);
  echo $state;
  exit;
}
/**
 * Page callback: Handling requestrer and requestee id.
 */
function my_contacts_handlingusercontacts($requesterid, $requesteeid, $contactstatus) {
  $state = FALSE;
  // If contact are addding.
  if ($contactstatus == 'add_to_contacts') {
    $resultset = db_query("INSERT INTO {contact_relationships} (requester_id, requestee_id, access) VALUES ('%d', '%d', 'Private')", $requesterid, $requesteeid);
    $state = 'Record sucessully added in your contact list.';
  }
  // If contact are removing.
  elseif ($contactstatus == 'remove_to_contacts') {
    $resultset = db_query("DELETE FROM {contact_relationships} WHERE requester_id = '%d' AND requestee_id='%d'", $requesterid, $requesteeid);
    $state = 'Record sucessully removed in your contact list.';
  }
  return $state;
}
/**
 * Page callback: To show dialog box.
 */
function my_contacts_confirmdialogbox() {
  $html = '<div id="overlay_form" style="display:none;">';
  $html .= '<div id="confirm-message">Do you want to continue ?</div>';
  $html .= '<input type="button" id="confirm-yes" value="Yes" />';
  $html .= '<input type="button" id="confirm-no" value="No" />';
  $html .= '<input type="hidden" id="requesteeid" value="" />';
  $html .= '<input type="hidden" id="contact_status" value="" />';
  $html .= '<input type="hidden" id="rem_contact" value="" />';
  $html .= '<input type="hidden" id="object-rem_contact" value="" />';
  $html .= '<a href="#" id="rem-list-confirm-close" >&nbsp;</a>';
  $html .= '</div>';
  return $html;
}
/**
 * Page callback: To show dialog box.
 */
function my_contacts_remove_confirmdialogbox() {
  $html = '<div id="rem_overlay_form" style="display:none;">';
  $html .= '<div id="rem-confirm-message">Do you want to remove this contact permanantly ?</div>';
  $html .= '<input type="button" id="rem-confirm-yes" value="Yes" />';
  $html .= '<input type="button" id="rem-confirm-no" value="No" />';
  $html .= '<input type="hidden" id="rem_contact" value="" />';
  $html .= '<a href="#" id="rem-confirm-close" onclick= "rem_close();" >&nbsp;</a>';
  $html .= '</div>';
  return $html;
}
/**
 * Page callback: Get all user from contact directory.
 */
function my_contacts_getalldiruser($status = 0, $pager = FALSE, $roles = array()) {
  global $user;
  // Sort by name by default, a-z order.
  $header = array(
    array(t('Choose')),
    array('data' => t('First Name'), 'field' => 'first_name', 'sor' => 'asc'),
    array('data' => t('Last Name'), 'field' => 'last_name'),
    array('data' => t('Email'), 'field' => 'email1'),
    array('data' => t('Organization'), 'field' => 'organization'),
  );
  if ($status == 0) {
    $intersect_array = array_intersect($roles, $user->roles);
    if (!empty($intersect_array) || $user->uid == 1) {
      $sql = "SELECT cdu.contact_id, cdu.first_name, cdu.last_name, cdu.email1, cdu.organization,uid FROM {contact_directory_user} as cdu  ";
      $resultset = db_query($sql . tablesort_sql($header));
    }
    else {
      $sql = "SELECT cdu.contact_id, cdu.first_name, cdu.last_name, cdu.email1, cdu.organization,uid FROM {contact_directory_user} as cdu INNER JOIN {contact_relationships} as cr ON cr.requestee_id = cdu.contact_id  WHERE ( (cr.access='%s') || (cr.access='%s' && cr.requester_id='%d')) ";
      $resultset = db_query($sql . tablesort_sql($header), 'Public', 'Private', $user->uid);
    }
  }
  elseif ($status == 1) {
    /* Getting list of my contact  */
    $sql = "SELECT cdu.contact_id, cdu.first_name, cdu.last_name, cdu.email1, cdu.organization, cdu.uid FROM {contact_directory_user} as cdu INNER JOIN {contact_relationships} as t1 ON t1.requestee_id = cdu.contact_id WHERE 	t1.requester_id = '%d'";
    $resultset = db_query($sql . tablesort_sql($header), $user->uid);
  }
  if ($pager == FALSE) {
    while ($result = db_fetch_object($resultset)) {
      $directory_user[] = array(
        'contact_id' => $result->contact_id,
        'first_name' => $result->first_name,
        'last_name' => $result->last_name,
        'email1' => $result->email1,
        'organization' => $result->organization,
        'uid' => $result->uid,
      );
    }
    return $directory_user;
  }
  else {
    return $sql;
  }
}
/**
 * Page callback: Get friend list of specefic user.
 */
function my_contacts_getfriendlist($uid) {
  $reuesterid = array();
  $sql = "SELECT requestee_id FROM {contact_relationships} WHERE requester_id ='%d'";
  $resultset = db_query($sql, $uid);
  while ($result = db_fetch_object($resultset)) {
    $reuesterid[] = $result->requestee_id;
  }
  return $reuesterid;
}
/**
 * Page callback: Get the list of contact which have access level equal to 'Private'.
 */
function my_contacts_getfriends_statuslist($uid) {
  $requesterid = array();
  $sql = "SELECT requestee_id FROM {contact_relationships} WHERE requester_id ='%d' AND access = '%s'";
  $resultset = db_query($sql, $uid, 'Private');
  while ($result = db_fetch_object($resultset)) {
    $requesterid[] = $result->requestee_id;
  }
  return $requesterid;
}
/**
 * Page callback: Autocomplete friends suggestion.
 */
function my_contacts_friends_autocomplete($string) {
  global $user;
  $matches = array();
  $sql = " SELECT uid, name FROM {users} WHERE LOWER(name) LIKE LOWER('%%%s%%') OR LOWER(mail) LIKE LOWER('%%%s%%') ";
  $resultset = db_query($sql, $string, $string);
  while ($result = db_fetch_object($resultset)) {
    $matches[$result->name] = check_plain($result->name);
  }
  // Return for JS.
  print drupal_to_js($matches);
  exit();
}
/**
 * Page callback: Ajax call function for check existence of contact for user.
 */
function my_contacts_contact_existence() {
  global $user;
  if (isset($_POST['id'])) {
    $uid = $user->uid;
    $contact_id = $_POST['id'];
    $sql = "SELECT count(*) FROM {contact_relationships} WHERE requester_id = '%d' AND requestee_id = '%d'";
    $contact_existence = db_result(db_query($sql, $uid, $contact_id));
    if ($contact_existence == 0) {
      echo 'not exist';
      exit;
    }
  }
}
/**
 * Page callback: Associate contacts with each other if mutual friend is making.
 */
function my_contacts_association_existence() {
  global $user;
  if (isset($_POST['id'])) {
    $uid = $user->uid;
    $contact_id = $_POST['id'];
    $sql = "SELECT email1 FROM {contact_directory_user} WHERE contact_id = '%d'";
    $contact_email = db_result(db_query($sql, $contact_id));
    $sql1 = "SELECT count(*) FROM {contact_directory_user} WHERE uid = '%d' AND email1 = '%s'";
    $association_existence = db_result(db_query($sql1, $uid, $contact_email));
    if ($association_existence == 0) {
      echo 'not exist';
      exit;
    }
  }
}
/**
 * Page callback: Show suggestion friend list.
 */
function my_contacts_sugest_friends_autocomplete() {
  global $user, $base_url;
  $username = $_GET['username'];
  $uid = my_contacts_user_load_by_username($username);
  if (!empty($username) && $username != '') {
    // Returning filter user friend list.
    $sql = "SELECT cr.requestee_id FROM {contact_relationships} as cr INNER JOIN {users} as u ON u.uid = cr.requester_id WHERE u.uid = '%d'";
    $resultset = db_query($sql, $user->uid);
    $filteruser = array();
    while ($result = db_fetch_object($resultset)) {
      $filteruser[$result->requestee_id] = $result->requestee_id;
    }
    // Returning all contact list.
    $allusersql = "SELECT cr.requestee_id as cid,cdu.first_name,cdu.last_name, cdu.email1,cdu.organization FROM {contact_relationships} as cr INNER JOIN {users} as u ON u.uid = cr.requester_id INNER JOIN {contact_directory_user} as cdu ON cdu.contact_id =cr.requestee_id  WHERE (" . $level;
    $alluser = array();
    $imgpath = check_url($base_url . '/' . drupal_get_path('module', 'my_contacts'));
    $header = array(
      array('data' => t('First Name')),
      array('data' => t('Last Name')),
      array('data' => t('Email')),
      array('data' => t('Organization')),
      array('data' => t('Operations')),
    );
    // Sort by name by default, a-z order.
    // If search user and input user uid are same means user are wathing contact ownself.
    if ($user->uid == $uid) {
      $level = $allusersql . " u.uid='%d' )";
      $alluserset = db_query($level . tablesort_sql($header), $uid);
    }
    else {
      $user_roles = my_contacts_get_user_roles();
      $intersect_array = array_intersect($user_roles, $user->roles);
      if ( !empty($intersect_array) || $user->uid == 1)  {
        $level = $allusersql . " u.uid='%d' )";
        $alluserset = db_query($level . tablesort_sql($header),  $uid);
      }
      else {
        $level = $allusersql . " cr.access='%s' && u.uid='%d' )";
        $alluserset = db_query($level . tablesort_sql($header), 'public', $uid);
      }
    }
    while ($alluserdata = db_fetch_object($alluserset)) {
      if (array_key_exists($alluserdata->cid, $filteruser)) {
        $action_value = $alluserdata->cid;
        $defaultclass = 'remove_to_contacts';
        $imgpaths = $imgpath . '/images/remove-contact.png';
      }
      else {
        $action_value = $alluserdata->cid;
        $defaultclass = 'add_to_contacts';
        $imgpaths = $imgpath . '/images/add-contact.png';
      }
      $edit_contact_img = "<img src='$imgpath/images/edit.png' />";
      $add_img = ' <img src="' . $imgpaths . '" class="' . $action_value . '" />';
      $valuet = l($add_img, ' ', array('fragment' => 'add_to_contact', 'external' => TRUE, 'html' => 'true', 'attributes' => array('id' => "$action_value", 'class' => 'makerequest', 'rel' => "$defaultclass")));
      $imgtag = '<a href="javascript:void(0)" onclick="makefriendrequeset(this.id, this.rel)" id="' . $action_value . '" class="makerequest" rel="' . $defaultclass . '" ><img src="' . $imgpaths . '" class="' . $action_value . '" /></a>';
      $alluser[] = array(
        'first_name' => $alluserdata->first_name,
        'last_name' => $alluserdata->last_name,
        'email1' => $alluserdata->email1,
        'organization' => $alluserdata->organization,
        'contact_id' => $imgtag,
      );
    }
    $rows = array();
    foreach ($alluser as $key => $val) {
      foreach ($val as $k => $v) {
        $data[] = $v;
      }
      $rows[] = $data;
      unset($data);
    }
  }
  print theme('table', $header, $rows, $attributes = array('class' => 'database-table-plugin'));
}
/**
 * Page callback: Ajax call function for change the access level of contact.
 */
function my_contacts_change_access() {
  global $user;
  if (isset($_POST['status']) && isset($_POST['id'])) {
    $contact_id = $_POST['id'];
    $uid = $user->uid;
    if ($_POST['status'] == 'Public') {
      $status = 'Private';
    }
    else {
      $status = 'Public';
    }
    $sql = "UPDATE {contact_relationships} SET access = '%s' WHERE requester_id = '%d' AND requestee_id = '%d'";
    db_query($sql, $status, $uid, $contact_id);
    echo $status;
    exit;
  }
}
/**
 * Page callback: Making friend request to another person.
 */
function my_contacts_making_friends_request() {
  global $user;
  $requesteeid = $_GET['requesteeId'];
  if ($_GET['requesteeId'] != '' && $_GET['status'] != '') {
    if ($_GET['status'] == 'add_to_contacts') {
      $resultset = db_query("INSERT INTO {contact_relationships} (requester_id, requestee_id, access, created) VALUES ('%d', '%d', '%s', '%d')", $user->uid, $requesteeid, 'Private', time());
      $state = 'Record sucessully added in your contact list.';
    }
    elseif ($_GET['status'] == 'remove_to_contacts') {
      $resultset = db_query("DELETE FROM {contact_relationships} WHERE requester_id = '%d' AND requestee_id='%d'", $user->uid, $requesteeid);
      $state = 'Record sucessully removed in your contact list.';
    }
  }
  exit;
}
/**
 * Page callback: Loading user id by user name.
 */
function my_contacts_user_load_by_username($name) {
  $result = db_fetch_object(db_query("SELECT uid FROM {users} WHERE name ='%s' ", $name));
  return $result->uid;
}
/**
 * Page callback: Import Contacts by csv file.
 */
function my_contacts_import_contatcs() {
  global $base_url;
  $module_path = check_url($base_url . '/' . drupal_get_path('module', 'my_contacts') . '/example/contact_directory.csv');
  $ct_status_message = $_SESSION['import_poppp'] == 1 ? $_SESSION['import_msg'] : '';
  $form['import_contact_form'] = array(
    '#type' => 'markup',
    '#value' => '<div id="import-contact-status-wrapper">' . $ct_status_message . '</div>',
  );
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['csv_file'] = array(
    '#type' => 'upload_element',
    '#file_validators' => array(
      'file_validate_extensions' => array('CSV'),
    ),
    '#description' => t('<span class="form-required">*</span> ' . l(t('download'), $module_path) . ' sample csv format to import new contacts in contact directory<ul><li>Please dont change csv format.</li> <li>First name, Last name, Email1 fields are required</li>'),
  );
  $form['upload_file_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#attributes' => array('onclick' => 'return import_submit_bt();'),
  );
  return $form;
}
/**
 * Page callback: Import Contacts by csv file validation.
 */
function my_contacts_import_contatcs_validate($form, &$form_state) {
  // Doing validation of csv file.
  if (!($form_state['values']['csv_file']->filemime == 'application/octet-stream' || $form_state['values']['csv_file']->filemime == 'application/vnd.ms-excel' || $form_state['values']['csv_file']->filemime == 'text/csv')) {
    $_SESSION['import_poppp'] = 1;
    $_SESSION['import_msg'] = '<div id="import-contact-status-wrapper-error" class="import-stat-msg">Only CSV extension is allowed !</div>';
  }
}
/**
 * Page callback: Submission form of Import Contacts by csv file.
 */
function my_contacts_import_contatcs_submit($form, &$form_state) {
  file_check_directory($d = file_directory_path() . '/my_contacts', FILE_CREATE_DIRECTORY);
  file_check_directory($d = file_directory_path() . '/my_contacts/import', FILE_CREATE_DIRECTORY);
  $directory = file_directory_path() . '/my_contacts/import';
  $file_fid = upload_element_save($form_state['values']['csv_file'], $directory, $replace = FILE_EXISTS_RENAME, $presetname = FALSE, $delete_original = TRUE);
  $csv_file_path = db_result(db_query('SELECT filepath FROM {files} WHERE fid = %d', $file_fid));
  $flag = my_contacts_file_encode_decode_nsv($csv_file_path);
  if ($flag['flag'] == TRUE || $flag['flag'] == 1) {
    $_SESSION['import_poppp'] = 1;
    $_SESSION['import_msg'] = "<div id='import-contact-status-wrapper-sucess' class='import-stat-msg'>Import statics - Import : <strong> " . $flag['success'] . " </strong> <span class='not-imported'> Not imported  <strong>" . $flag['failure'] . " </strong></span></div>";
    if ($flag['failure'] > 0) {
      $_SESSION['import_msg'] .= "<div class='not-import'>Your contacts have not import it may be below resons : <ul class='import-reason'><li>Require fields are blank.</li><li>Email address are not valid.</li><li>Email address should be unique.</li><ul></div>";
    }
  }
  else {
    $_SESSION['import_poppp'] = 1;
    $_SESSION['import_msg'] = '<div id="import-contact-status-wrapper-error">You may change your file format.Please upload correct csv format file!</div>';
  }
}
/**
 * Page callback: Destroying all sessions.
 */
function my_contacts_hide_import_popup() {
  unset($_SESSION['import_poppp']);
  unset($_SESSION['import_msg']);
}
/**
 * Page callback: File read and imporing contacts in our list.
 */
function my_contacts_file_encode_decode_nsv($file_path) {
  $csv_file_path = $file_path;
  // Open the file for reading.
  $file_handle = fopen($csv_file_path, 'r');
  $form_state = array();
  $i = 0;
  $flag = TRUE;
  $success = 0;
  $failure = 0;
  // Get file line as csv.
  while ($csv_line = fgetcsv($file_handle)) {
    if ($i == 0) {
      if ($csv_line[0] != 'First Name' && $csv_line[1] != 'Last Name' && $csv_line[2] != 'Email1' && $csv_line[3] != 'Prefix' && $csv_line[4] != 'Suffix' && $csv_line[5] != 'Access' && $csv_line[6] != 'Language' && $csv_line[7] != 'Notes' && $csv_line[8] != 'Email2' && $csv_line[9] != 'Country' && $csv_line[10] != 'Address' && $csv_line[11] != 'Mobile' && $csv_line[12] != 'Organization') {
        $flag = FALSE;
        break;
      }
    }
    if ($i != 0) {
      $form_state['contact_fname'] = $csv_line[0];
      $form_state['contact_lname'] = $csv_line[1];
      $form_state['contact_email1'] = $csv_line[2];
      $form_state['contact_prefix'] = $csv_line[3];
      $form_state['contact_suffix'] = $csv_line[4];
      $form_state['contact_language'] = $csv_line[5];
      $form_state['contact_notes'] = $csv_line[6];
      $form_state['contact_email2'] = $csv_line[7];
      $country_key = my_contacts_contact_directory_get_iso3166_list(FALSE, FALSE, $csv_line[8]);
      $form_state['contact_country'] = $country_key;
      $form_state['contact_address'] = $csv_line[9];
      $form_state['contact_pcell'] = $csv_line[10];
      $form_state['contact_organization'] = $csv_line[11];
      $cid = my_contacts_add_or_edit_contact_details($form_state, 'insert');
      $cid > 0 ? $success++ : $failure++;
    }
    $i++;
  }
  $flag_array = array(
    'flag' => $flag,
    'success' => $success,
    'failure' => $failure,
  );
  return $flag_array;
}
/**
 * Page callback: Remove contacts by moderator.
 * Public and it is shared then it cant delete.
 * If user contact is public and but it not shared then it will delete.
 * If user is private then it will delete.
 */
function my_contacts_remove_contatcs() {
  global $user;
  $contact_id = $_GET['contact_id'];
  if ($contact_id != '') {
    $checkcid = db_fetch_object(db_query("SELECT contact_id FROM {contact_directory_user} WHERE contact_id = '%d'", $contact_id));
    if (empty($checkcid->contact_id)) {
      print json_encode(array('msg' => 'This contact is already deleted by another user .', 'status' => 1));
    }
    else {
      contact_delete_by_cid($contact_id);
      print json_encode(array('msg' => 'Your contact is sucessfully removed.', 'status' => 1));
    }
    exit;
  }
}
/**
 * Page callback: Contact delete by contact id.
 */
function my_contacts_contact_delete_by_cid($cid) {
  db_query("DELETE FROM {contact_directory_user} WHERE contact_id = '%d'", $cid);
  db_query("DELETE FROM {contact_relationships} WHERE  requestee_id = '%d'", $cid);
}
