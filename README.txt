My Contacts is an ajaxified contact management module that empowers 
your registered users of to maintain their personal contacts. 

This module comes with useful features that are missing  in the similar 3rd 
party modules. 

Some of the  key features:
1. Add/Edit Contacts
2. Pull contacts of other users
3. Lookup  contacts from CSV
4. Ajaxified contact searching and filtering 
5. Manage Contact Profiles
6. User can maintain public and private contacts
7. Administrator can manage all the contacts of users (public & private both).

Dependencies
-------------
1. Upload elements
2. ImageCache
3. ImageAPI
4. ImageAPI GD2 (This module is ImageAPI's dependency)

INSTALLATION

--------------
1. Copy the contact directory module to your modules directory and enable it on 
the Modules
   page (admin/build/modules).
2. Give some roles permission to Add/Manage contacts at the Access control page
   (admin/user/access). The following permissions can be controlled:
    administer contact directory - Allows users to access private contacts of 
    all the users
    contont dirctory access - Allows to add/manage his contacts
