var basePath, success_add_msg, success_remove_msg, rem_close, rd_contacts, remove_permanat_contact, import_submit_bt;
success_add_msg = 'Contact is succuessfull added in your contact list.';
success_remove_msg = 'Contact is succuessfull removed in your contact list.';
Drupal.behaviors.my_contacts = function () {
    $(document).ready(function () {
        basePath = Drupal.settings.my_contacts.my_setting + '/';
        $('.database-table-plugin thead tr th a').each(function () {
            //Stoping page refresh at sorting time.
            $(this).attr('href', 'javascript:void()');
        });
        //Making Table sortable and instantly searching and pagingnation
        oTable = $('.database-table-plugin').dataTable({
            "sPaginationType": "full_numbers"
        });
        $('thead.tableHeader-processed tr th.sorting_asc').removeAttr("class");
        /* Open friends popup as confirmation dialog box. */
          rd_contacts = function (elm) {
            //requestee id
            var contact_id = elm.id;
            var requesteeStatus = $(elm).children('img').attr('class');
            var fields = {};
            var association_exist;
            fields['id'] = contact_id;
            if (requesteeStatus != 'remove_to_contacts') {
                $.ajax({
                    type: "POST",
                    url: basePath + "check_association_contact",
                    data: fields,
                    async: false,
                    success: function (data) {
                        association_exist = data;
                    }
                });
            } else {
                association_exist = 'remove_to_contacts';
            }
            if (association_exist == 'not exist' || association_exist == 'remove_to_contacts') {
                $('#contact_status').val(requesteeStatus);
                $('#requesteeid').val(contact_id);
                $("#overlay_form").fadeIn('fast');
                positionPopup("overlay_form");
                $("body").append('<div class="modalOverlay">');
            } else {
                alert('This contact is already added in your directory');
                return false;
            }
        }
        //close popup
        $("#overlay_form #rem-list-confirm-close, #overlay_form  #confirm-no").click(function () {
            $('#requesteeid').val('');
            $('#contact_status').val('');
            $("#overlay_form").fadeOut('fast');
            $(".modalOverlay").remove();
        });
        //If yes button click
        $('#confirm-yes').click(function () {
            var requesteeid = $('#requesteeid').val();
            var contact_status = $('#contact_status').val();
            var imagepath;
            var classpath;
            var msg;
            if (contact_status == 'add_to_contacts') {
                imagepath = basePath + 'sites/all/modules/my_contacts/images/remove-contact.png';
                classpath = 'remove_to_contacts';
                msg = success_add_msg;
            } else if (contact_status == 'remove_to_contacts') {
                imagepath = basePath + 'sites/all/modules/my_contacts/images/add-contact.png';
                classpath = 'add_to_contacts';
                msg = success_remove_msg;
            }
            $.ajax({
                type: "GET",
                url: basePath + "add_to_contact?requesteeid=" + requesteeid + '&contact_status=' + contact_status,
                success: function (data) {
                    //if(data=='TRUE'){
                    $('#requesteeid').val('');
                    $('#contact_status').val('');
                    $("#overlay_form").fadeOut('fast');
                    $('#' + requesteeid + '  img').attr('src', imagepath);
                    $('#' + requesteeid + '  img').attr('class', classpath);
                    $(".modalOverlay").remove();
                    $('.set-message').fadeIn('fast').html(msg);
                    setTimeout(function () {
                        // Do something after 5 seconds
                        $('.set-message').fadeOut('fast');
                    }, 3000);

                    //}
                }
            });
        });
        /* open popup of friends list of their contacts... */
        $('.add_to_contacts_friends').click(function () {
            var contactid = this.id;
            var requesteedid = $('#requesteeid1').val(contactid);
            $("#overlay_form2").fadeIn('fast');
            positionPopup_frnd();
            $('#edit-add-user-friends-list').focus();
            $("body").append('<div class="modalOverlay">');
        });
        //Close registerd list of user's popup
        $("#overlay_form2 #confirm-close2").click(function () {
            $("#overlay_form2").fadeOut('fast');
            //Reseting all values .
            $('#edit-add-user-friends-list').val('');
            $('#suggest-friend-list').html('');
            location.reload();
        });
        oTable1 = $('#table-plugin-user-reference').dataTable({
            "sPaginationType": "full_numbers"
        });
        var suggestfriend = function (username) {
            if (username != '') {
                $('#loader').show();
                $('#requesteeid_frnd').html($(this).val()).wrap('<strong>');
                $.ajax({
                    type: "GET",
                    url: basePath + "sugeestfriendlist?username=" + username,
                    success: function (data) {
                        $('#suggest-friend-list').html(data);
                        var width = $('#overlay_form2').width();
                        $('#confirm-close2').css('left', width);
                        $('#loader').hide();
                    }
                });
            }

        }
        $('#search_con_by_user').click(function () {
            var username = $('#edit-add-user-friends-list').val();
            suggestfriend(username);
        });
        //Show own contact
        $('#edit-show-myaccount').click(function () {
            //Sending request and reciving response from ajax requeset.
            var status, acc_url;
            if ($('#edit-show-myaccount').attr('checked') == true) {
                status = 1;
                acc_url = basePath + "browse_contact/status/" + status;
            } else {
                status = 0;
                acc_url = basePath + "browse_contact/status/" + status;
            }
            location.href = acc_url;
            $.ajax({
                type: "GET",
                url: acc_url,
                success: function (data) {

                }
            });
        });
        //remove contact popup
        remove_permanat_contact = function (elm) {
            $("#rem_overlay_form").fadeIn('fast');
            positionPopup("rem_overlay_form");
            $("body").append('<div class="modalOverlay">');
            var contact_id = $(elm).attr('rel');
            $('#object-rem_contact').val($(elm));
            $('#rem_contact').val(contact_id);
        }
        rem_close = function () {
            $("#rem_overlay_form").fadeOut("fast");
            //Reseting all values .
            $('#rem_contact').val('');
            $(".modalOverlay").remove();
        };
        //remove contact dialog box at 'No' button click
        $('#rem-confirm-no').click(function () {
            rem_close();

        })
        //remove contact dialog box at 'Yes' button click
        $('#rem-confirm-yes').click(function () {
            var contact_id = $('#rem_contact').val();
            var object = $('.user_remove[rel=' + contact_id + ']');
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: basePath + "remove_contact?contact_id=" + contact_id,
                success: function (data) {
                    if (data.status == 1){
                        object.parent('td').parent('tr').hide();
                    };
                    $('.set-message').fadeIn('fast').html(data.msg);
                    //$('.set-message').delay(5000);
                    setTimeout(function () {
                        // Do something after 5 seconds
                        $('.set-message').fadeOut('fast');
                    }, 3000);
                    $("#rem_overlay_form").fadeOut('fast');
                    $('#rem_contact').val('');
                    $(".modalOverlay").remove();
                }
            });
        });
        $('.imports_contact_dir').click(function () {
            $("body").append('<div class="modalOverlay">');
            $('.loader').show();
            setTimeout(function () {
                $('.loader').hide();
                $("#import_contact").fadeIn("fast");
                positionPopup('import_contact');

            }, 200);
        })
        $('#import-close').click(function () {
            $.ajax({
                type: "GET",
                url: basePath + '/import_contact_hide',
                success: function (data) {

                }
            });
            $("#import_contact").fadeOut("fast");
            $('.import-stat-msg').val();
            $(".modalOverlay").remove();
            $('.upload-element-preview').html('---');
            $('#edit-csv-file-action-wrapper').remove();
            $('#import-contact-status-wrapper').html('');
            $('#edit-file-csv-file').val('')
        })
        import_submit_bt = function () {
            var lens;
            lens = $('.upload-element-preview').html().length;
            if (lens < 4) {
                alert('Befor submit form please select a file.')
                return false;
            }
            return true;
        }

    });
} //End

function makefriendrequeset(id, rel) {
    var requesteeid = id;
    var status = rel;
    var msg;
    var association_exist;
    var fields = {};
    fields['id'] = requesteeid;
    $.ajax({
        type: "POST",
        url: basePath + "check_association_contact",
        data: fields,
        async: false,
        success: function (data) {
            association_exist = data;
        }
    });
    $('#loader').show();
    if (status == 'add_to_contacts') {
        imagepath = basePath + 'sites/all/modules/my_contacts/images/remove-contact.png';
        classpath = 'remove_to_contacts';
        msg = success_add_msg;

    } else if (status == 'remove_to_contacts') {
        imagepath = basePath + 'sites/all/modules/my_contacts/images/add-contact.png';
        classpath = 'add_to_contacts';
        msg = success_remove_msg;
    }
    if (association_exist == 'not exist') {
        $.ajax({
            type: "GET",
            url: basePath + "makingrequest?requesteeId=" + requesteeid + "&status=" + status,
            success: function (data) {
                $('#suggest-friend-list a#' + requesteeid + ' img').attr('src', imagepath);
                $('#suggest-friend-list a#' + requesteeid).attr('rel', classpath)
                $('#loader').hide();
                $('.set-message-outer').fadeIn('fast').html(msg);
                //$('.set-message').delay(5000);
                setTimeout(function () {
                    // Do something after 5 seconds
                    $('.set-message-outer').fadeOut('fast');
                }, 3000);
            }
        });
    } else {
        $('#loader').hide();
        alert('This contact is already added in your directory');
        return false;
    }
}
//position the popup at the center of the page

function positionPopup(id) {
    var poppup_id = '#' + id;
    if (!$(poppup_id).is(':visible')) {
        return;
    }
    $(poppup_id).css({
        left: ($(window).width() - $(poppup_id).width()) / 2,
        top: ($(window).width() - $(poppup_id).width()) / 7,
        position: 'absolute'
    });
}
//position the popup at the center of the page

function positionPopup_frnd() {
    if (!$("#overlay_form2").is(':visible')) {
        return;
    }
    $("#overlay_form2").css({
        left: ($(window).width() - $('#overlay_form2').width()) / 2,
        top: ($(window).width() - $('#overlay_form2').width()) / 7,
        position: 'absolute'
    });
}
