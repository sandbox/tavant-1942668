var pub_prv_contact;

function removeContactComment(commentID) {
 var basePath = Drupal.settings.my_contacts.my_setting;
 var fields = {};
 var r = confirm("Are you sure want to delete this comment");
 if (r == true) {
  fields['commentId'] = commentID;
  $.ajax({
   type : "POST",
   url : basePath + "/remove_contact_comment",
   data : fields,
   success : function(data) {
    console.log(data);
    if (data == 1) {
     $('#comment_' + commentID).parent().parent().remove();
    }
   }
  });
 } else {
  return false;
 }
}
$(document).ready(
  function() {
   var basePath = Drupal.settings.my_contacts.my_setting;
   var urlPath = Drupal.settings.basePath;
   var public_add_msg = 'Contact is succuessfull become public for others.';
   var private_add_msg = 'Contact is succuessfull become private for you.';
   var existence;
   $("#comment-legend").click(function() {
    if ($("#contenor-form-comment").is(":hidden")) {
     $("#content-comment").removeClass("collapsed");
     $("#contenor-form-comment").show('slow');

    } else {
     $("#content-comment").addClass("collapsed");
     $("#contenor-form-comment").slideUp(2000, 'linear');

    }
   });
   $(document).ajaxComplete(function() {
    $("#edit-comment-textbox").val('');
   });
   Drupal.behaviors.my_contacts = function() {
    $("#edit-contact-pictures-action").click(function() {
     if ($(this).attr('checked') == true) {
      alert('Please click on Update Picture to discard upload')
     }
    });
   }
   pub_prv_contact = function(elm) {
    // requestee id
    var id = elm.id;
    var id_split = id.split("_");
    var contact_id = id_split[1];
    var requesteeStatus = $(elm).children('img').attr('class');
    var status_imagepath;
    var status_class;
    var msg;
    var alert_msg;
    var r;
    var fields = {};
    fields['status'] = requesteeStatus;
    fields['id'] = contact_id;
    $.ajax({
     type : "POST",
     url : urlPath + "contact-existence",
     data : fields,
     async : false,
     success : function(data) {
      existence = data;
     }
    });
    if (requesteeStatus == 'Public') {
     // alert('jknjk')
     status_imagepath = basePath
 + '/sites/all/modules/my_contacts/images/private.png';
     status_class = 'Private';
     msg = private_add_msg;
     alert_msg = 'Do you want to make it private to you ?';
    } else {
     // alert('jknjkknmlkm')
     status_imagepath = basePath
 + '/sites/all/modules/my_contacts/images/public.png';
     classpath = 'public';
     msg = public_add_msg;
     alert_msg = 'Do you want to make it public for others ?';
    }
    if (existence != 'not exist') {
     r = confirm(alert_msg);
     if (r == true) {
      $.ajax({
       type : "POST",
       url : urlPath + "change-access-level",
       data : fields,
       success : function(data) {
        $('#status_' + contact_id + '  img').attr('src', status_imagepath);
        $('#status_' + contact_id + '  img').attr('class', data);
        $('#status_' + contact_id + '  img').attr('title', data);
        $('.set-message').fadeIn('slow').html(msg);
        setTimeout(function() {
         // Do something after 5 seconds
         $('.set-message').fadeOut('slow');
        }, 3000);
       }
      });
     } else {
      return false;
     }
    } else {
     alert('First add this contact into your directory');
    }
   }
  });
