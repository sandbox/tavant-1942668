<?php
/**
 * @file
 * Template file for the display of add edit contact form of user list.
 */
global $user, $base_url;
?>
<div id="addPeople">
	<div id="error"></div>
	<div id="setupform">
		<table class="main-table-contact">
			<tr>
				<td colspan="2">
					<fieldset class="basic-info-fieldset">
						<legend>Basic Information</legend>
						<table class="basic-info-table" cellspacing="0" cellpadding="0">
							<tr>
								<td rowspan="6">
									<div class="profile-pic-div">

										<div class="<?php (arg(0) == 'edit-people') ? '' : 'profile-pic';?>">
											<?php
                                            ($form['html-element']) ? drupal_render($form['html-element']) : '';?>
											<?php print drupal_render($form['contact_pictures'])?>
										</div>
									</div>
								</td>
								<td><?php print drupal_render($form['contact_fname'])?></td>
							</tr>
							<tr>
								<td><?php print drupal_render($form['contact_lname'])?></td>
							</tr>
							<tr>
								<td><?php print drupal_render($form['contact_prefix'])?></td>
							</tr>
							<tr>
								<td><?php print drupal_render($form['contact_suffix'])?></td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<fieldset>
						<legend>Contact Information</legend>
						<table class="basic-info-table" cellspacing="0" cellpadding="0">
							<tr>
								<td><?php print drupal_render($form['contact_email1'])?></td>
								<td class="mailingAddress"><?php print drupal_render($form['contact_address'])?>
								</td>
							</tr>
							<tr>
								<td><?php print drupal_render($form['contact_email2'])?></td>
								<td><?php print drupal_render($form['contact_pcell'])?></td>
							</tr>
							<tr>
								<td><?php print drupal_render($form['contact_country'])?></td>
								<td><?php print drupal_render($form['contact_organization'])?></td>

						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<fieldset>
						<legend>Other Information</legend>
						<table class="basic-info-table" cellspacing="0" cellpadding="0">
							<tr>
								<td class="vert-al-top"><?php print drupal_render($form['contact_language'])?>
								</td>
								<td colspan="2" class="mailingAddress"><?php print drupal_render($form['contact_notes'])?>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
	</div>
	<div class="save-button-people">
		<?php print drupal_render($form['submit'])?>
	</div>
	<div style='display: none;'>
		<?php print drupal_render($form)?>
	</div>
</div>
