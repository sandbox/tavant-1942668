<?php
/**
 * @file
 * Template file for the display of contact directory user list.
 */
global $base_url;
$add_contact_img = '<img src="' . $base_url . '/' . path_to_theme() . '/images/add-to-contact1.png" title = "Add Contact"/>';
$import_csv_img = '<img src="' . $base_url . '/' . path_to_theme() . '/images/csv_import.png" title = "Import Csv"/>';
// Confirmation dilog box.
print '<div class="contact-top">';
print "<div class='show-my-contact'>" . drupal_render($form['show_myaccount']) . "</div>";
print "<div class='more-contacts'>" . drupal_render($form['add_more_contacts']) . "</div>";
print "<div class='more-contacts'><div id='import_contact' class='overlay_form' style='display:none;'>" . drupal_get_form('my_contacts_import_contatcs') . "<a href='javascript:void(0)' id='import-close' >&nbsp;</a></div>" . l($import_csv_img, ' ', array('fragment' => 'import_user_contact', 'html' => TRUE, 'external' => TRUE, 'attributes' => array('class' => 'imports_contact_dir'))) . "</div>";
print "<div class='more-contacts'>" . l($add_contact_img, 'add-contact', array('html' => 'true')) . "</div>";
print '</div>';
print '<div class="set-message">this is test message</div>';
print '<div style="clear:both;"><div>';
print drupal_render($form['add_to_contact_form']);
print drupal_render($form['rem_to_contact_form']);
// Add to other contacts.
$html  = '<div id="overlay_form2" style="display:none;">';
$html .= '<div id="confirm-message">Contact list of registered users</div>';
$html .= drupal_render($form['add_user_friends_list']);
$html .= '<input type="button" name="search_contact_by_user" id="search_con_by_user" value="Search" />';
$html .= '<div class="set-message-outer"></div>';
$html .= '<div id="loader" style="display:none"><img src="' . $base_url . '/' . path_to_theme() . '/images/loadingBar.gif" /></div>';
$html .= '<input type="hidden" id="requesteeid1" value="" />';
$html .= '<input type="hidden" id="contact_status" value="" />';
$html .= '<a href="#" id="confirm-close2" >&nbsp;</a>';
$html .= '<div id="suggest-friend-list"></div>';
$html .= '</div>';
print $html;
// Sort by name by default, a-z order.
$header = array(
  array('data' => t('First Name'), 'field' => 'first_name', 'sor' => 'asc'),
  array('data' => t('Last Name'), 'field' => 'last_name'),
  array('data' => t('Email'), 'field' => 'email1'),
  array('data' => t('Organization'), 'field' => 'organization'),
  array('data' => t('Operations')),
);
$rows = array();
$total_column = $form['total_column']['#value'];
$i = 0;
foreach (element_children($form) as $val) {
  if ($i > 0) {
    $data[] = drupal_render($form[$val]);
  }
  if ($i % $total_column == 0 && $i != 0) {
    $rows[] = $data;
    unset($data);
  }
  $i++;
}
if ($rows[0][0] === ''|| $rows[0][0] == '') {
  print '<div class="no-recordes">' . t('There are no records added now.') . '</div>';
}
else {
  print theme('table', $header, $rows, $attributes = array('class' => 'database-table-plugin'));
}
  print '<div style="display:none;">' . drupal_render($form) . '</div>';
