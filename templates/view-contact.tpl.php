<?php 
/**
 * @file
 * Template file for the display of contact directory user list.
 */
global $base_url, $user;
?>
<!-- To new profile page provided by prince  -->
<div id="addPeople">
  <div class="top-header-view-contact">
		<span class="text"><?php print $contact->first_name . '&nbsp;' . $contact->last_name?></span>
  		<div class="edit-profile-button">
			<a href="<?php print $base_url . '/edit-contact/' . $contact->contact_id; ?>">Edit Contact</a>
		</div>
	</div>	
	<div id="setupform" class="setup-profile-view">
		<div class="profile-view">
			<fieldset class="profile-view-fieldset profile-basic-info-fieldset">
				<div class="head">Basic Information</div>
				<ul class="profile-view-ul">
					<li>
						<div class="form-item" id="edit-contact-fname-wrapper">
							<label for="edit-contact-fname">First Name:</label>
							<div class="form-text required"><?php echo $contact->first_name;?></div> 
						</div>
					</li>
					<li>
						<div class="form-item" id="edit-contact-lname-wrapper">
								<label for="edit-contact-lname">Last Name: </label>
								<div class="form-text required"><?php echo $contact->last_name;?></div>
						</div>
					</li>
					<?php if ($contact->prefix != '') {?>
					<li>
						<div class="form-item" id="edit-contact-prefix-wrapper">
							<label for="edit-contact-prefix">Prefix Name: </label>
							<div class="form-text"><?php echo $contact->prefix;?></div>
						</div>
					</li>
					<?php } ?>
					<?php if ($contact->suffix != '') {?>
					<li>
						<div class="form-item" id="edit-contact-suffix-wrapper">
							<label for="edit-contact-suffix">Suffix Name: </label>
							<div class="form-text"><?php echo $contact->suffix;?></div>
						</div>
					</li>
					<?php } ?>
				</ul>
				<div class="<?php echo $contact->class_id;?>">
					<div>
						<img src="<?php echo $contact->image;?>" >
					</div>
				</div>
			</fieldset>

			<fieldset class="profile-view-fieldset profile-view-fieldset-contact-info">
				<div class="head">Contact Information</div>
				<ul class="profile-view-ul">
					<li>
						<div class="form-item" id="edit-contact-email1-wrapper">
							<label for="edit-contact-email1">Email 1: </label> 
							<div class="form-text required"><a class="mailto" style="width:auto;" href="mailto:<?php print $contact->email1?>" title = "<?php print $contact->email1;?>" ><?php echo $contact->email1;?></a></div>
						</div>
					</li>
					<?php if ($contact->email2 != '') {?>
					<li>
						<div class="form-item" id="edit-contact-email2-wrapper">
							<label for="edit-contact-email2">Email 2: </label> 
							<div class="form-text"><a href="mailto:<?php print $contact->email2;?>" title = "<?php print $contact->email2;?>"><?php echo $contact->email2;?></a></div>
						</div>
					</li>
					<?php } ?>
					<?php if ($contact->country != '') {?>
					<li>
						<div class="form-item" id="edit-contact-country-wrapper">
							<label for="edit-contact-country">Country : </label>
							<div class="form-text required"><?php if ($contact->country) {echo $contact->country;}?></div>
						</div>
					</li>
					<?php } ?>
					<?php if ($contact->mobile != 0) {?>
					<li>
						<div class="form-item" id="edit-contact-pcell-wrapper">
							<label for="edit-contact-pcell">Mobile : </label>
							<div class="form-text required"><?php if ($contact->mobile) {echo $contact->mobile;}?></div>
						</div>
					</li>
                    <?php } ?>	
                    <?php if ($contact->organization != '') {?>					
					<li><div class="form-item" id="edit-contact-orgnization-wrapper">
							<label for="edit-contact-orgnization">Organization: </label> 
							<div class="form-text required"><?php echo $contact->organization;?></div>
						</div>
					</li>
					<?php } ?>
					<?php if ($contact->address != '') {?>
					<li>
						<div class="form-item" id="edit-contact-address-wrapper">
							<label for="edit-contact-address">Mailing Address: </label>
							<div class="form-text required"><?php echo $contact->address;?></div>
						</div>
					</li>
					<?php } ?>
				</ul>
			</fieldset>
			<?php if ($contact->language != '' || $contact->notes != '') {?>	
			<fieldset class="profile-view-fieldset">
				<div class="head">Other Information</div>
				<ul class="profile-view-ul profile-other-info-ul">
				<?php if ($contact->language != '') {?>
					<li>
                         <div class="form-item" id="edit-contact-language-wrapper">
							<label for="edit-contact-language">Preferred Language:</label>
							<div class="form-text"><?php echo $contact->language;?></div>
						</div>
					</li>
				<?php } ?>
				<?php if ($contact->notes != '') {?>
					<li>
						<div class="form-item" id="edit-contact-notes-wrapper">
							<label for="edit-contact-notes">Notes: </label>
							<div class="form-text"><?php echo $contact->notes;?></div>
						</div>
					</li>
					<?php } ?>
				</ul>
			</fieldset>
			<?php } ?>
			<fieldset class="profile-view-fieldset comment-section-profile" id="content-comment">
				<div class="head head-arrow-down" id="comment-list-header">
					<?php echo t("Post Comments"); ?>
				</div>
				<div id="contenor-form-comment">
				<div id="contenor-comment">
						<?php echo $html; ?>
					</div>
						<div class="story-sections">
							<div class="story-sec-detail doc-comment" id="comment-box">
								<div class="comment_box">
									<?php print $form;?>
								</div>
							</div>
						</div>	
					</div>
			</fieldset>
		</div>
	</div>
</div>
